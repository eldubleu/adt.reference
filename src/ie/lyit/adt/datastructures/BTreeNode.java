package ie.lyit.adt.datastructures;

import java.util.Arrays;
import java.util.Comparator;

/**
 * BTree node
 * 
 * @author markus.korbel@lyit.ie
 * 
 * @param <T>
 *            The data type of the keys stored in the tree nodes
 */
public class BTreeNode<T extends Comparable<T>> {
	/**
	 * The keys stored in this node (m, 1 temporary spare before splits)
	 */
	private T[] keys = null;

	/**
	 * The number of keys stored in this node (>m/2 and <m-1)
	 */
	private int keyCount = 0;

	/**
	 * The parent of this node (root: null)
	 */
	private BTreeNode<T> parent = null;

	/**
	 * The children of this node (m+1, 1 temporary spare before splits)
	 */
	private BTreeNode<T>[] children = null;

	/**
	 * The number of children (min. 2 (except root), up to m)
	 */
	private int childCount = 0;

	/**
	 * A custom comparator than can compare to child nodes (based on key[0])
	 */
	private Comparator<BTreeNode<T>> childComparator = new Comparator<BTreeNode<T>>() {
		/**
		 * Compares to nodes based on key[0]
		 * 
		 * @param node1
		 *            The first node
		 * @param node2
		 *            The second node
		 * @return Default <0,0,>0 comparator result
		 */
		@Override
		public int compare(BTreeNode<T> node1, BTreeNode<T> node2) {
			return node1.getKey(0).compareTo(node2.getKey(0));
		}
	};

	/**
	 * Constructor
	 * 
	 * @param parent
	 *            The parent of the new node
	 * @param treeOrder
	 *            The order of the BTree (m)
	 */
	@SuppressWarnings("unchecked")
	public BTreeNode(BTreeNode<T> parent, int treeOrder) {
		this.parent = parent;
		this.keys = (T[]) new Comparable[treeOrder];
		this.children = new BTreeNode[treeOrder + 1];
	}

	/**
	 * Get the key stored at the specified index
	 * 
	 * @param index
	 *            The index
	 * @return The key at index
	 */
	public T getKey(int index) {
		return this.keys[index];
	}

	/**
	 * Gets the index of the specified value
	 * 
	 * @param value
	 *            The value to look for
	 * @return The index of the value, or -1 if the value is not stored in this
	 *         node
	 */
	public int indexOf(T value) {
		for (int i = 0; i < this.keyCount; i++) {
			if (this.keys[i].equals(value)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Adds the key to the node (unchecked!, check numberOfKeys() first!)
	 * 
	 * @param value
	 *            The value to add
	 */
	public void addKey(T value) {
		this.keys[this.keyCount++] = value;
		Arrays.sort(this.keys, 0, this.keyCount);
	}

	/**
	 * Removes the specified key
	 * 
	 * @param value
	 *            The key to remove
	 * @return The key removed, or NULL if the key is not stored in this node
	 */
	public T removeKey(T value) {
		T removed = null;
		if (this.keyCount > 0) {
			for (int i = 0; i < this.keyCount; i++) {
				if (this.keys[i].equals(value)) {
					removed = this.keys[i];
				} else if (removed != null) {
					// Shift the rest of the keys forward
					this.keys[i - 1] = this.keys[i];
				}
			}
		}
		// Remove the last element and reduce the key count
		if (removed != null) {
			this.keyCount--;
			this.keys[this.keyCount] = null;
		}
		return removed;
	}

	/**
	 * Removes the key at the specified index position
	 * 
	 * @param index
	 *            The index of the key to remove
	 * @return The key removed, or NULL if there was no key stored at this index
	 */
	public T removeKey(int index) {
		if (index >= this.keyCount) {
			return null;
		}
		T value = keys[index];
		for (int i = index + 1; i < this.keyCount; i++) {
			// Shift the rest of the keys forward
			this.keys[i - 1] = this.keys[i];
		}
		this.keyCount--;
		this.keys[this.keyCount] = null;
		return value;
	}

	/**
	 * Gets the number of keys stored in the node
	 * 
	 * @return The number of keys stored in the node
	 */
	public int numberOfKeys() {
		return this.keyCount;
	}

	/**
	 * Gets the child at the specified index
	 * 
	 * @param index
	 *            The index of the child to return
	 * @return The child at the index, or NULL if there is no child at this
	 *         index
	 */
	public BTreeNode<T> getChild(int index) {
		if (index >= this.childCount) {
			return null;
		}
		return this.children[index];
	}

	/**
	 * Gets the index of the specified child
	 * 
	 * @param child
	 *            The child to look for
	 * @return The index of the child (in this parent node), or -1 if the
	 *         specified node is not a child of this node
	 */
	public int indexOf(BTreeNode<T> child) {
		for (int i = 0; i < this.childCount; i++) {
			if (this.children[i].equals(child)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Adds the specified node as a child of this node (Unchecked!, check
	 * numberOfChildren() first!)
	 * 
	 * @param child
	 *            The child to add
	 */
	public void addChild(BTreeNode<T> child) {
		child.parent = this;
		this.children[this.childCount++] = child;
		Arrays.sort(this.children, 0, this.childCount, this.childComparator);
	}

	/**
	 * Removes the specified child node
	 * 
	 * @param child
	 *            The child node to remove
	 * @return True if the node was found and removed, false if the specified
	 *         node is not a child of this node
	 */
	public boolean removeChild(BTreeNode<T> child) {
		boolean found = false;
		for (int i = 0; i < this.childCount; i++) {
			if (this.children[i].equals(child)) {
				found = true;
			} else if (found) {
				// Shift the rest of the children forwards
				this.children[i - 1] = this.children[i];
			}
		}
		// If we found the child, reduce the count and set the last to NULL
		if (found) {
			this.childCount--;
			this.children[this.childCount] = null;
		}
		return found;
	}

	/**
	 * Removes the child at the specified index
	 * 
	 * @param index
	 *            The index of the child to remove
	 * @return The child node that was removed, or NULL if there is no child at
	 *         that index
	 */
	public BTreeNode<T> removeChild(int index) {
		if (index >= this.childCount) {
			return null;
		}
		BTreeNode<T> value = this.children[index];
		for (int i = index + 1; i < this.childCount; i++) {
			// Shift the rest of the children forwards
			this.children[i - 1] = this.children[i];
		}
		this.childCount--;
		this.children[this.childCount] = null;
		return value;
	}

	/**
	 * Gets the number of children of this node
	 * 
	 * @return The number of children of this node
	 */
	public int numberOfChildren() {
		return this.childCount;
	}

	/**
	 * Gets the child nodes of this one
	 * 
	 * @return The child nodes
	 */
	public BTreeNode<T>[] getChildren() {
		return this.children;
	}

	/**
	 * Gets the parent of this node
	 * 
	 * @return The parent of this node, or NULL if this node is the root
	 */
	public BTreeNode<T> getParent() {
		return this.parent;
	}

	/**
	 * Sets a new parent for this node
	 * 
	 * @param parent
	 *            The new parent node
	 */
	public void setParent(BTreeNode<T> parent) {
		this.parent = parent;
	}

	/**
	 * BTreeNode toString method
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append("keys=[");
		for (int i = 0; i < numberOfKeys(); i++) {
			T value = this.getKey(i);
			builder.append(value);
			if (i < this.numberOfKeys() - 1) {
				builder.append(", ");
			}
		}
		builder.append("]\n");

		if (this.parent != null) {
			builder.append("parent=[");
			for (int i = 0; i < this.parent.numberOfKeys(); i++) {
				T value = this.parent.getKey(i);
				builder.append(value);
				if (i < this.parent.numberOfKeys() - 1)
					builder.append(", ");
			}
			builder.append("]\n");
		}

		if (this.children != null) {
			builder.append("keySize=").append(this.numberOfKeys())
					.append(" children=").append(this.numberOfChildren())
					.append("\n");
		}

		return builder.toString();
	}
}
