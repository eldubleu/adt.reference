package ie.lyit.adt.datastructures;

/**
 * BTree data structure
 * 
 * @author markus.korbel@lyit.ie
 * 
 * @param <T>
 *            The data type of the keys stored in the tree
 */
public class BTree<T extends Comparable<T>> {
	/**
	 * The minimum key count for nodes (except the root) (m/2)
	 */
	private int minKeyCount = 2;

	/**
	 * Minimum child count for internal nodes (except root) (minKeyCount + 1)
	 */
	private int minChildCount = 3;

	/**
	 * Maximum key count per node (m-1)
	 */
	private int maxKeyCount = 4;

	/**
	 * The maximum number of children any node can have (m .. order of BTree)
	 */
	private int maxChildCount = 5;

	/**
	 * The root of the tree
	 */
	private BTreeNode<T> root = null;

	/**
	 * The size of the tree
	 */
	private int size = 0;

	/**
	 * Default constructor (BTree of order m=5)
	 */
	public BTree() {
		// Just using default values
	}

	/**
	 * Creates a BTree of specific order
	 * 
	 * @param order
	 *            The order of the BTree to create
	 */
	public BTree(int order) {
		this.minKeyCount = order / 2;
		this.minChildCount = this.minKeyCount + 1;
		this.maxKeyCount = order - 1;
		this.maxChildCount = order;
	}

	/**
	 * Add a key to the tree
	 * 
	 * @param value
	 *            The key to add
	 */
	public void add(T value) {
		// TODO

		this.size++;
	}

	/**
	 * Splits the specified (over populated) node
	 * 
	 * @param node
	 *            The node to split
	 */
	private void split(BTreeNode<T> node) {
		// TODO
	}

	/**
	 * Removes the specified key from the tree
	 * 
	 * @param value
	 *            The key to remove
	 * @return The key if was removed, or NULL if the key was not found in the
	 *         tree
	 */
	public T remove(T value) {
		T removed = null;
		BTreeNode<T> node = this.getNode(value);
		removed = remove(value, node);
		return removed;
	}

	/**
	 * Removes the specified key from the specified node
	 * 
	 * @param value
	 *            The key to remove
	 * @param node
	 *            The node to remove it from
	 * @return The key if it was removed, NULL otherwise
	 */
	private T remove(T value, BTreeNode<T> node) {
		// TODO
		
		this.size--;
		return null;
	}

	/**
	 * Remove greatest valued key from a node
	 * 
	 * @param node
	 *            The node from which to remove greatest the value from
	 * @return The value removed (or NULL if there are no keys in the node)
	 */
	private T removeGreatestValue(BTreeNode<T> node) {
		T value = null;
		if (node.numberOfKeys() > 0) {
			value = node.removeKey(node.numberOfKeys() - 1);
		}
		return value;
	}

	/**
	 * Get the greatest valued child from a node
	 * 
	 * @param node
	 *            The node from which to return the greatest child
	 * @return The child with the greatest value (largest key[0]) or NULL if the
	 *         node has no children
	 */
	private BTreeNode<T> getGreatestNode(BTreeNode<T> node) {
		while (node.numberOfChildren() > 0) {
			node = node.getChild(node.numberOfChildren() - 1);
		}
		return node;
	}

	/**
	 * Combines the specified node with it's children
	 * 
	 * @param node
	 *            The node from which to start the combine process
	 */
	private void combine(BTreeNode<T> node) {
		BTreeNode<T> parent = node.getParent();
		int index = parent.indexOf(node);
		int indexOfLeftNeighbor = index - 1;
		int indexOfRightNeighbor = index + 1;

		BTreeNode<T> rightNeighbour = null;
		int rightNeighborSize = -this.minKeyCount;
		if (indexOfRightNeighbor < parent.numberOfChildren()) {
			rightNeighbour = parent.getChild(indexOfRightNeighbor);
			rightNeighborSize = rightNeighbour.numberOfKeys();
		}

		// Try to borrow neighbour
		if (rightNeighbour != null && rightNeighborSize > this.minKeyCount) {
			// Try to borrow from right neighbour
			T removeValue = rightNeighbour.getKey(0);
			int prev = getIndexOfPreviousValue(parent, removeValue);
			T parentValue = parent.removeKey(prev);
			T neighborValue = rightNeighbour.removeKey(0);
			node.addKey(parentValue);
			parent.addKey(neighborValue);
			if (rightNeighbour.numberOfChildren() > 0) {
				node.addChild(rightNeighbour.removeChild(0));
			}
		} else {
			BTreeNode<T> leftNeighbour = null;
			int leftNeighbourSize = -this.minKeyCount;
			if (indexOfLeftNeighbor >= 0) {
				leftNeighbour = parent.getChild(indexOfLeftNeighbor);
				leftNeighbourSize = leftNeighbour.numberOfKeys();
			}

			if (leftNeighbour != null && leftNeighbourSize > this.minKeyCount) {
				// Try to borrow from left neighbour
				T removeValue = leftNeighbour.getKey(leftNeighbour
						.numberOfKeys() - 1);
				int prev = getIndexOfNextValue(parent, removeValue);
				T parentValue = parent.removeKey(prev);
				T neighborValue = leftNeighbour.removeKey(leftNeighbour
						.numberOfKeys() - 1);
				node.addKey(parentValue);
				parent.addKey(neighborValue);
				if (leftNeighbour.numberOfChildren() > 0) {
					node.addChild(leftNeighbour.removeChild(leftNeighbour
							.numberOfChildren() - 1));
				}
			} else if (rightNeighbour != null && parent.numberOfKeys() > 0) {
				// Can't borrow from neighbours, try to combined with right
				// neighbor
				T removeValue = rightNeighbour.getKey(0);
				int prev = getIndexOfPreviousValue(parent, removeValue);
				T parentValue = parent.removeKey(prev);
				parent.removeChild(rightNeighbour);
				node.addKey(parentValue);
				for (int i = 0; i < rightNeighbour.numberOfKeys(); i++) {
					T v = rightNeighbour.getKey(i);
					node.addKey(v);
				}
				for (int i = 0; i < rightNeighbour.numberOfChildren(); i++) {
					BTreeNode<T> c = rightNeighbour.getChild(i);
					node.addChild(c);
				}

				if (parent.getParent() != null
						&& parent.numberOfKeys() < this.minKeyCount) {
					// Removing key made parent too small, combined up tree
					this.combine(parent);
				} else if (parent.numberOfKeys() == 0) {
					// Parent no longer has keys, make this node the new root
					// which decreases the height of the tree
					node.setParent(null);
					this.root = node;
				}
			} else if (leftNeighbour != null && parent.numberOfKeys() > 0) {
				// Can't borrow from neighbours, try to combined with left
				// neighbour
				T removeValue = leftNeighbour.getKey(leftNeighbour
						.numberOfKeys() - 1);
				int prev = getIndexOfNextValue(parent, removeValue);
				T parentValue = parent.removeKey(prev);
				parent.removeChild(leftNeighbour);
				node.addKey(parentValue);
				for (int i = 0; i < leftNeighbour.numberOfKeys(); i++) {
					T v = leftNeighbour.getKey(i);
					node.addKey(v);
				}
				for (int i = 0; i < leftNeighbour.numberOfChildren(); i++) {
					BTreeNode<T> c = leftNeighbour.getChild(i);
					node.addChild(c);
				}

				if (parent.getParent() != null
						&& parent.numberOfKeys() < this.minKeyCount) {
					// Removing key made parent too small, combined up tree
					this.combine(parent);
				} else if (parent.numberOfKeys() == 0) {
					// Parent no longer has keys, make this node the new root
					// which decreases the height of the tree
					node.setParent(null);
					this.root = node;
				}
			}
		}
	}

	/**
	 * Get the index of previous key in node.
	 * 
	 * @param node
	 *            The node to find the previous key in.
	 * @param value
	 *            The key to find a previous value for.
	 * @return The index of previous key or -1 if not found.
	 */
	private int getIndexOfPreviousValue(BTreeNode<T> node, T value) {
		for (int i = 1; i < node.numberOfKeys(); i++) {
			T t = node.getKey(i);
			if (t.compareTo(value) >= 0)
				return i - 1;
		}
		return node.numberOfKeys() - 1;
	}

	/**
	 * Get the index of next key in node
	 * 
	 * @param node
	 *            The node to find the next key in
	 * @param value
	 *            The key to find a next value for
	 * @return The index of next key or -1 if not found
	 */
	private int getIndexOfNextValue(BTreeNode<T> node, T value) {
		for (int i = 0; i < node.numberOfKeys(); i++) {
			T t = node.getKey(i);
			if (t.compareTo(value) >= 0)
				return i;
		}
		return node.numberOfKeys() - 1;
	}

	/**
	 * Clears the tree
	 */
	public void clear() {
		root = null;
		size = 0;
	}

	/**
	 * Checks if the tree contains the specified key
	 * 
	 * @param value
	 *            The key to search for
	 * @return True if the key is contained, false if not
	 */
	public boolean contains(T value) {
		BTreeNode<T> node = getNode(value);
		return (node != null);
	}

	/**
	 * Gets the node containing the specified key
	 * 
	 * @param value
	 *            The key to search for
	 * @return The node containing the key, or NULL if the key was not found in
	 *         the tree
	 */
	private BTreeNode<T> getNode(T value) {
		// Implement me
		return null;
	}

	/**
	 * Gets the size of the tree
	 * 
	 * @return The size of the tree
	 */
	public int getSize() {
		return this.size;
	}

	/**
	 * Validate the tree
	 * 
	 * @return True if the tree is valid, false otherwise
	 */
	public boolean validate() {
		if (root == null)
			return true;
		return validateNode(root);
	}

	/**
	 * Validate the node according to the B-Tree invariants
	 * 
	 * @param node
	 *            The node to validate
	 * @return True if valid, false otherwise
	 */
	private boolean validateNode(BTreeNode<T> node) {
		int keySize = node.numberOfKeys();
		if (keySize > 1) {
			// Make sure the keys are sorted
			for (int i = 1; i < keySize; i++) {
				T p = node.getKey(i - 1);
				T n = node.getKey(i);
				if (p.compareTo(n) > 0)
					return false;
			}
		}
		int childrenSize = node.numberOfChildren();
		if (node.getParent() == null) {
			// Root
			if (keySize > this.maxKeyCount) {
				// Check max key size. root does not have a min key size
				return false;
			} else if (childrenSize == 0) {
				// If root, no children, and keys are valid
				return true;
			} else if (childrenSize < 2) {
				// Root should have zero or at least two children
				return false;
			} else if (childrenSize > this.maxChildCount) {
				return false;
			}
		} else {
			// Non-root
			if (keySize < this.minKeyCount) {
				return false;
			} else if (keySize > this.maxKeyCount) {
				return false;
			} else if (childrenSize == 0) {
				return true;
			} else if (keySize != (childrenSize - 1)) {
				// If there are children, there should be one more child then
				// keys
				return false;
			} else if (childrenSize < this.minChildCount) {
				return false;
			} else if (childrenSize > this.maxChildCount) {
				return false;
			}
		}

		BTreeNode<T> first = node.getChild(0);
		// The first child's last key should be less than the node's first key
		if (first.getKey(first.numberOfKeys() - 1).compareTo(node.getKey(0)) > 0)
			return false;

		BTreeNode<T> last = node.getChild(node.numberOfChildren() - 1);
		// The last child's first key should be greater than the node's last key
		if (last.getKey(0).compareTo(node.getKey(node.numberOfKeys() - 1)) < 0)
			return false;

		// Check that each node's first and last key holds it's invariance
		for (int i = 1; i < node.numberOfKeys(); i++) {
			T p = node.getKey(i - 1);
			T n = node.getKey(i);
			BTreeNode<T> c = node.getChild(i);
			if (p.compareTo(c.getKey(0)) > 0)
				return false;
			if (n.compareTo(c.getKey(c.numberOfKeys() - 1)) < 0)
				return false;
		}

		for (int i = 0; i < node.numberOfChildren(); i++) {
			BTreeNode<T> c = node.getChild(i);
			boolean valid = this.validateNode(c);
			if (!valid)
				return false;
		}

		return true;
	}

	/**
	 * Prints the tree to stdout
	 */
	public void printTree() {
		System.out.println(TreePrinter.getString(this));
	}

	/**
	 * Tree printer helper class
	 * 
	 * @author markus.korbel@lyit.ie
	 * 
	 */
	private static class TreePrinter {
		/**
		 * Gets a formatted string for the tree
		 * 
		 * @param tree
		 *            The tree to print
		 * @return A formatted string for the tree
		 */
		public static <T extends Comparable<T>> String getString(BTree<T> tree) {
			if (tree.root == null)
				return "Tree has no nodes.";
			return getString(tree.root, "", true);
		}

		/**
		 * Creates a formatted string for the specified node
		 * 
		 * @param node
		 *            The node to print
		 * @param prefix
		 *            The current prefix
		 * @param isTail
		 *            Is this node a tail?
		 * @return The formatted string
		 */
		private static <T extends Comparable<T>> String getString(
				BTreeNode<T> node, String prefix, boolean isTail) {
			StringBuilder builder = new StringBuilder();

			builder.append(prefix).append((isTail ? "└── " : "├── "));
			for (int i = 0; i < node.numberOfKeys(); i++) {
				T value = node.getKey(i);
				builder.append(value);
				if (i < node.numberOfKeys() - 1)
					builder.append(", ");
			}
			builder.append("\n");

			if (node.getChildren() != null) {
				for (int i = 0; i < node.numberOfChildren() - 1; i++) {
					BTreeNode<T> obj = node.getChild(i);
					builder.append(getString(obj, prefix
							+ (isTail ? "    " : "│   "), false));
				}
				if (node.numberOfChildren() >= 1) {
					BTreeNode<T> obj = node
							.getChild(node.numberOfChildren() - 1);
					builder.append(getString(obj, prefix
							+ (isTail ? "    " : "│   "), true));
				}
			}

			return builder.toString();
		}
	}
}
